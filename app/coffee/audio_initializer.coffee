module.exports = class AudioInitializer
  constructor: ->
    @audioElement = $('#stream').get(0)
    @AddCanPlayListener()

    @loading = false
    @loaded = false
    @playing = false

    @StopAndUnloadAudio()

  ToggleAudio: =>
    if @playing
      @StopAndUnloadAudio()
    else
      @LoadAndPlayAudio()

  StopAndUnloadAudio: =>
    clearTimeout @loadCheckTimeout
    clearInterval @repeatGetData
    $('#artist span').text('')
    $('#title span').text('')

    @audioElement.pause()
    @originalSrc = @audioElement.src
    @audioElement.src = 'about:blank'
    @audioElement.load()
    $('#stream').remove()
    $('#audioContainer').append("<audio id='stream' preload='none' crossorigin='anonymous'></audio>")
    @audioElement = $('#stream').get(0)
    @audioElement.src = @originalSrc

    @AddCanPlayListener()

    @loaded = false
    @loading = false
    @playing = false

    $('#playIcon').removeClass('iconHide')
    $('#stopIcon').addClass('iconHide')

    return

  LoadAndPlayAudio: =>
    @loading = true
    $('#audioContainer audio').attr('preload','auto')
    @audioElement.load()
    $('#playIcon').addClass('iconHide')
    $('#loadingAnimation').addClass('drip')
    clearTimeout @loadCheckTimeout
    @loadCheckTimeout = setTimeout @CheckLoaded, 8000

    return

  CheckLoaded: =>
    if @loading
      @audioElement.load()
      @loadCheckTimeout = setTimeout @CheckLoaded, 8000

    return

  AddCanPlayListener: =>
    @audioElement.addEventListener 'canplay', =>
      @loaded = true
      @loading = false

      $('#loadingAnimation').removeClass('drip')
      $('#stopIcon').removeClass('iconHide')

      @playing = true
      @audioElement.play()
      @GetIcecastData()
      @repeatGetData = setInterval @GetIcecastData, 8000

      return

    return

  GetIcecastData: =>
    $.ajax({
      url: 'http://168.235.77.138:8000/status-json.xsl',
      type: 'GET',
      success: (data) =>
        @UpdateText(data.icestats.source.title)
      failure: (status) ->
        console.log('status: ' + status)
      dataType: 'json',
      timeout: 2000
    })

    return

  UpdateText: (songData) =>
    #still broken if song has dash in it but not multiple artsts
    # maybe check for duplication of artist name instead and base it on that
    if (@CountOccurrences(songData, ' - ') < 1)
      $('#track span').text('station id')
    else if (@CountOccurrences(songData, ' - ') == 1)
      @artistName = songData.split(' - ')[0]
      @songName = songData.split(' - ')[1]
    else
      artistSubStringLocation = @GetNthOccurrence(songData, ' - ', 1)
      songSubStringLocation = @GetNthOccurrence(songData, ' - ', 2)
      @artistName = songData.substring(artistSubStringLocation + 3, songSubStringLocation)
      @songName = songData.substring(songSubStringLocation + 3, songData.length)

    @UpdateTextDisplay()

    return

  UpdateTextDisplay: =>
    $('#artist span').text(@artistName)
    $('#title span').text(@songName)
    return

  GetNthOccurrence: (str, m, i) ->
    return str.split(m, i).join(m).length

  CountOccurrences: (str, value) ->
    regExp = new RegExp(value, "gi")
    return (str.match(regExp) || []).length
