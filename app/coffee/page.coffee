AudioInitializer = require('coffee/audio_initializer')

module.exports = class Page
  constructor: ->
    @audioInitializer = new AudioInitializer()
    @AddAudioToggleListener()

    return

  AddAudioToggleListener: =>
    $('.toggle').click =>
      @audioInitializer.ToggleAudio()
      return

    return
